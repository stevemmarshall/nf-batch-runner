# Running a pipeline
1. To run a pipeline select a pipeline from the dropdown and fill in the parameters as required.
File paths should be specifies as paths within a S3 bucket prefixed using `s3://`
![UI submit pipeline](images/ui-submit-pipeline.png "Submitting a pipeline")Press submit

2. The webpage will monitor the pipeline and update the page with progress
![UI monitoring pipeline - pending](images/ui-pipeline-monitoring-pending.png "Monitoring a pipeline - pending")
![UI monitoring pipeline - starting](images/ui-pipeline-monitoring-starting.png "Monitoring a pipeline - starting")
![UI monitoring pipeline - running](images/ui-pipeline-monitoring-running.png "Monitoring a pipeline - running")
![UI monitoring pipeline - success](images/ui-pipeline-monitoring-success.png "Monitoring a pipeline - success")
3. Outputs will be available for download from the S3 output bucket
![UI download outputs](images/ui-pipeline-download.png "Download outputs")

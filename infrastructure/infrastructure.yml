Description: Creates an environment in which you can run Nextflow pipelines

Outputs:
  FilesBucket:
    Description: S3 bucket used for pipeline inputs and outputs
    Value: !Ref FilesBucket
    Export:
      Name: !Sub "${AWS::StackName}-FilesBucket"

  UsersDb:
    Description: Database containing pipeline users
    Value: !Ref UsersDb
    Export:
      Name: !Sub "${AWS::StackName}-UsersDb"

  PipelinesDb:
    Description: Database containing pipelines
    Value: !Ref PipelinesDb
    Export:
      Name: !Sub "${AWS::StackName}-PipelinesDb"

  PipelinesDbStream:
    Description: Pipelines database events
    Value: !GetAtt "PipelinesDb.StreamArn"
    Export:
      Name: !Sub "${AWS::StackName}-PipelinesDbStream"

  FrontendUrl:
    Condition: ShouldDeployFrontend
    Value: !GetAtt "FrontendDistribution.DomainName"
    Description: URL for frontend

  FrontendBucket:
    Condition: ShouldDeployFrontend
    Value: !Ref FrontendBucket
    Description: S3 bucket for the frontend

Parameters:
  WorkerNodeAMI:
    Type: AWS::EC2::Image::Id
    Description: AMI used by AWS Batch
    Default: ami-0d58587c6ec7936ce

  DeployFrontend:
    Type: String
    Description: Should we deploy the frontend
    Default: "no"
    AllowedValues:
      - "yes"
      - "no"
    ConstraintDescription: Must be "yes" or "no"

  FrontendStaticPrefix:
    Type: String
    Description: Optional prefix for frontend artifacts in the S3 bucket
    Default: "/"

  SessionSecret:
    Type: String
    Description: Secret used to protect sessions
    NoEcho: true

Conditions:
  ShouldDeployFrontend: !Equals [ !Ref DeployFrontend, "yes" ]

Resources:
  ##############################
  # Create the VPC, networks etc
  ##############################

  # VPC config is taken from https://docs.aws.amazon.com/codebuild/latest/userguide/cloudformation-vpc-template.html
  # Retrieved 6 March 2019

  VPC:
    Type: AWS::EC2::VPC
    Properties:
      CidrBlock: "10.0.0.0/16"
      EnableDnsSupport: true
      EnableDnsHostnames: true

  InternetGateway:
    Type: AWS::EC2::InternetGateway

  InternetGatewayAttachment:
    Type: AWS::EC2::VPCGatewayAttachment
    Properties:
      InternetGatewayId: !Ref InternetGateway
      VpcId: !Ref VPC

  PublicSubnet1:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      AvailabilityZone: !Select [ 0, !GetAZs '' ]
      CidrBlock: "10.0.0.0/24"
      MapPublicIpOnLaunch: true

  PublicSubnet2:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      AvailabilityZone: !Select [ 1, !GetAZs '' ]
      CidrBlock: "10.0.32.0/24"
      MapPublicIpOnLaunch: true

  PublicRouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref VPC

  DefaultPublicRoute:
    Type: AWS::EC2::Route
    DependsOn: InternetGatewayAttachment
    Properties:
      RouteTableId: !Ref PublicRouteTable
      DestinationCidrBlock: 0.0.0.0/0
      GatewayId: !Ref InternetGateway

  PublicSubnet1RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PublicRouteTable
      SubnetId: !Ref PublicSubnet1

  PublicSubnet2RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PublicRouteTable
      SubnetId: !Ref PublicSubnet2

  NoIngressSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupName: "no-ingress-sg"
      GroupDescription: "Security group with no ingress rule"
      VpcId: !Ref VPC

  WebIngressSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupName: "web-ingress-sg"
      GroupDescription: "Security group for web traffic"
      VpcId: !Ref VPC
      SecurityGroupIngress:
      - CidrIp: 0.0.0.0/0
        FromPort: 80
        IpProtocol: tcp
        ToPort: 80
      - CidrIp: 0.0.0.0/0
        FromPort: 443
        IpProtocol: tcp
        ToPort: 443

  ManagementIngressSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupName: "management-ingress-sg"
      GroupDescription: "Security group for management traffic"
      VpcId: !Ref VPC

  ##############################
  # Create the databases
  ##############################
  UsersDb:
    Type: AWS::DynamoDB::Table
    Properties:
      AttributeDefinitions:
        - AttributeName: user
          AttributeType: S
      BillingMode: PAY_PER_REQUEST
      KeySchema:
        - AttributeName: user
          KeyType: HASH

  PipelinesDb:
    Type: AWS::DynamoDB::Table
    Properties:
      AttributeDefinitions:
        - AttributeName: pipeline
          AttributeType: S
        - AttributeName: user
          AttributeType: S
        - AttributeName: created
          AttributeType: N
      BillingMode: PAY_PER_REQUEST
      KeySchema:
        - AttributeName: pipeline
          KeyType: HASH
      StreamSpecification:
        StreamViewType: NEW_AND_OLD_IMAGES
      GlobalSecondaryIndexes:
      - IndexName: user-created-index
        KeySchema:
        - AttributeName: user
          KeyType: HASH
        - AttributeName: created
          KeyType: RANGE
        Projection:
          ProjectionType: ALL
        ProvisionedThroughput:
          ReadCapacityUnits: 0
          WriteCapacityUnits: 0

  HistoryDb:
    Type: AWS::DynamoDB::Table
    Properties:
      AttributeDefinitions:
        - AttributeName: hash
          AttributeType: S
      BillingMode: PAY_PER_REQUEST
      KeySchema:
        - AttributeName: hash
          KeyType: HASH

  ProgressDb:
    Type: AWS::DynamoDB::Table
    Properties:
      AttributeDefinitions:
        - AttributeName: pipeline
          AttributeType: S
        - AttributeName: task
          AttributeType: S
      BillingMode: PAY_PER_REQUEST
      KeySchema:
        - AttributeName: pipeline
          KeyType: HASH
        - AttributeName: task
          KeyType: RANGE
      TimeToLiveSpecification:
        AttributeName: expires
        Enabled: true

  ##############################
  # Create the pipeline
  ##############################

  HeadNodeCluster:
    Type: AWS::ECS::Cluster
    Properties:
      Tags:
        - Key: nf-batch-runner
          Value: !Sub 'ProgressApi-${AWS::StackName}'

  HeadNodeClusterEvents:
    Type: AWS::Events::Rule
    Properties:
      EventPattern:
        source:
          - "aws.ecs"
        detail:
          clusterArn:
            - !GetAtt "HeadNodeCluster.Arn"
          stopCode:
            - "EssentialContainerExited"
      Targets:
        - Arn: !GetAtt "ProgressLambda.Arn"
          Id: ProgressLambdaV1

  HeadNodeClusterEventsPermission:
    Type: AWS::Lambda::Permission
    Properties:
      FunctionName: !Ref ProgressLambda
      Action: "lambda:InvokeFunction"
      Principal: "events.amazonaws.com"
      SourceArn: !GetAtt "HeadNodeClusterEvents.Arn"

  HeadNodeLogGroup:
    Type: "AWS::Logs::LogGroup"
    Properties:
      LogGroupName: !Sub "/aws/ecs/${AWS::StackName}-NextflowLogs"
      RetentionInDays: 1

  # Based on template linked to from: https://aws.amazon.com/blogs/compute/using-aws-cloudformation-to-create-and-manage-aws-batch-resources/
  # Retrieved 7 March 2019

  FilesBucket:
    Type: AWS::S3::Bucket
    Properties:
      AccessControl: Private
      LifecycleConfiguration:
        Rules:
        - ExpirationInDays: 90
          Id: Remove log files
          Prefix: logDir/
          Status: Enabled
        - ExpirationInDays: 30
          Id: Remove working files
          Prefix: workDir/
          Status: Enabled
        - ExpirationInDays: 7
          Id: Keep for 7 days
          Prefix: tmp/7days/
          Status: Enabled
        - ExpirationInDays: 30
          Id: Keep for 30 days
          Prefix: tmp/30days/
          Status: Enabled
        - ExpirationInDays: 60
          Id: Keep for 60 days
          Prefix: tmp/60days/
          Status: Enabled
        - ExpirationInDays: 90
          Id: Keep for 90 days
          Prefix: tmp/90days/
          Status: Enabled
    DeletionPolicy: Retain

  BatchServiceRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
        - Action: "sts:AssumeRole"
          Effect: Allow
          Principal:
            Service: batch.amazonaws.com
      ManagedPolicyArns:
      - 'arn:aws:iam::aws:policy/service-role/AWSBatchServiceRole'

  NextflowComputeEnvironment:
    Type: AWS::Batch::ComputeEnvironment
    Properties:
      ComputeResources:
        # Ec2KeyPair: bt5_20170711_sanger
        # ^^ DEBUG: you can provide the name of an SSH key here for debugging
        SpotIamFleetRole: !Ref NextflowSpotFleetRole
        BidPercentage: 30
        Type: SPOT
        ImageId: !Ref WorkerNodeAMI
        InstanceRole: !Ref WorkerNodeProfile
        InstanceTypes:
        - m4.large
        - m4.xlarge
        - m4.2xlarge
        - m4.4xlarge
        - m5.large
        - m5.xlarge
        - m5.2xlarge
        DesiredvCpus: 0
        MinvCpus: 0
        MaxvCpus: 64
        # Change me if you want to run more pipelines at once (and in HeadNodeLaunchConfig)
        # You need 2 x the maximum concurrency
        SecurityGroupIds:
        - !Ref NoIngressSecurityGroup
        Subnets:
        - !Ref PublicSubnet1
        - !Ref PublicSubnet2
        Tags: { "nf-batch-runner":  !Sub 'ProgressApi-${AWS::StackName}' }
      ServiceRole: !Ref BatchServiceRole
      Type: MANAGED

  NextflowSpotFleetRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
        - Action: "sts:AssumeRole"
          Effect: Allow
          Principal:
            Service: spotfleet.amazonaws.com
          Sid: ''
      ManagedPolicyArns:
      - 'arn:aws:iam::aws:policy/service-role/AmazonEC2SpotFleetAutoscaleRole'
      - 'arn:aws:iam::aws:policy/service-role/AmazonEC2SpotFleetRole'
      - 'arn:aws:iam::aws:policy/service-role/AmazonEC2SpotFleetTaggingRole'

  WorkerNodeRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: '2008-10-17'
        Statement:
        - Action: "sts:AssumeRole"
          Effect: Allow
          Principal:
            Service: ec2.amazonaws.com
          Sid: ''
      ManagedPolicyArns:
      - 'arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role'

  WorkerNodePolicy:
    Type: AWS::IAM::Policy
    Properties:
      PolicyName: WorkerNodeRole
      PolicyDocument:
        Statement:
        - Effect: Allow
          Action:
          - "s3:*"
          Resource: !GetAtt "FilesBucket.Arn"
        - Effect: Allow
          Action:
          - "s3:*"
          Resource: !Sub "${FilesBucket.Arn}/*"
      Roles:
      - Ref: WorkerNodeRole

  WorkerNodeProfile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      Roles:
      - !Ref WorkerNodeRole

  NextflowJobQueue:
    Type: AWS::Batch::JobQueue
    Properties:
      ComputeEnvironmentOrder:
      - ComputeEnvironment: !Ref NextflowComputeEnvironment
        Order: 1
      Priority: 1

  HeadNodeRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: '2008-10-17'
        Statement:
        - Action: "sts:AssumeRole"
          Effect: Allow
          Principal:
            Service:
              - ec2.amazonaws.com
              - ecs-tasks.amazonaws.com
          Sid: ''
      ManagedPolicyArns:
      - 'arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role'

  HeadNodePolicy:
    Type: AWS::IAM::Policy
    Properties:
      PolicyName: HeadNodeRole
      PolicyDocument: &headNodePolicyDocument
        Statement:
        - Effect: Allow
          Action:
          - "s3:*"
          Resource: !GetAtt "FilesBucket.Arn"
        - Effect: Allow
          Action:
          - "s3:*"
          Resource: !Sub "${FilesBucket.Arn}/*"
        - Effect: "Allow"
          Action:
          - "batch:*"
          - "cloudwatch:GetMetricStatistics"
          - "ec2:DescribeSubnets"
          - "ec2:DescribeSecurityGroups"
          - "ec2:DescribeKeyPairs"
          - "ec2:DescribeVpcs"
          - "ec2:DescribeImages"
          - "ec2:DescribeLaunchTemplates"
          - "ec2:DescribeLaunchTemplateVersions"
          - "ecs:DescribeClusters"
          - "ecs:Describe*"
          - "ecs:List*"
          - "ecs:StopTask"
          - "logs:Describe*"
          - "logs:Get*"
          - "logs:TestMetricFilter"
          - "logs:FilterLogEvents"
          - "iam:ListInstanceProfiles"
          - "iam:ListRoles"
          Resource: "*"
        - Effect: "Allow"
          Action:
          - "iam:PassRole"
          Resource:
          - "arn:aws:iam::*:role/AWSBatchServiceRole"
          - "arn:aws:iam::*:role/service-role/AWSBatchServiceRole"
          - "arn:aws:iam::*:role/ecsInstanceRole"
          - "arn:aws:iam::*:instance-profile/ecsInstanceRole"
          - "arn:aws:iam::*:role/iaws-ec2-spot-fleet-role"
          - "arn:aws:iam::*:role/aws-ec2-spot-fleet-role"
          - "arn:aws:iam::*:role/AWSBatchJobRole*"
        - Effect: Allow
          Action:
            - iam:ListAttachedRolePolicies
          Resource: "*"
        - Effect: "Allow"
          Action:
          - "iam:PassRole"
          Resource:
          - !GetAtt "HeadNodeRole.Arn"
        - Effect: Allow
          Action:
          - "ecs:RegisterTaskDefinition"
          Resource: "*"
        - Effect: Allow
          Action:
          - "ecs:RunTask"
          Resource: !Sub "arn:aws:ecs:${AWS::Region}:${AWS::AccountId}:task-definition/nf-run-*"
      Roles:
      - Ref: HeadNodeRole
      - Ref: PipelinesLambdaRole

  HeadNodeProfile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      Path: "/"
      Roles:
      - Ref: HeadNodeRole

  HeadNodeUser:
    Type: AWS::IAM::User
    Properties:
      Policies:
        - PolicyName: HeadNodeUserPolicies
          PolicyDocument: *headNodePolicyDocument

  HeadNodeUserKeys:
    Type: AWS::IAM::AccessKey
    Properties:
      UserName: !Ref HeadNodeUser

  ##############################
  # Create the Progress API
  ##############################

  # Inspired by https://gist.github.com/magnetikonline/c314952045eee8e8375b82bc7ec68e88
  # by Peter Mescalchin

  ProgressApi:
    Type: "AWS::ApiGateway::RestApi"
    Properties:
      Name: !Sub 'ProgressApi-${AWS::StackName}'
      Description: !Sub 'Collects progress reports from ${AWS::StackName}'

  ProgressApiRootMethod:
    Type: AWS::ApiGateway::Method
    Properties:
      RestApiId: !Ref ProgressApi
      ResourceId: !GetAtt "ProgressApi.RootResourceId"
      HttpMethod: ANY
      AuthorizationType: NONE
      Integration:
        Type: AWS_PROXY
        IntegrationHttpMethod: POST
        Uri: !Sub arn:aws:apigateway:${AWS::Region}:lambda:path/2015-03-31/functions/${ProgressLambda.Arn}/invocations

  ProgressApiProxyResource:
    Type: AWS::ApiGateway::Resource
    Properties:
      RestApiId: !Ref ProgressApi
      ParentId: !GetAtt "ProgressApi.RootResourceId"
      PathPart: '{proxy+}'

  ProgressApiProxyMethod:
    Type: AWS::ApiGateway::Method
    Properties:
      RestApiId: !Ref ProgressApi
      ResourceId: !Ref ProgressApiProxyResource
      HttpMethod: ANY
      AuthorizationType: NONE
      Integration:
        Type: AWS_PROXY
        IntegrationHttpMethod: POST
        Uri: !Sub arn:aws:apigateway:${AWS::Region}:lambda:path/2015-03-31/functions/${ProgressLambda.Arn}/invocations

  ProgressApiDeployment:
    Type: AWS::ApiGateway::Deployment
    DependsOn:
      - ProgressApiRootMethod
      - ProgressApiProxyMethod
    Properties:
      RestApiId: !Ref ProgressApi
      StageName: default

  ProgressLambda:
    Type: AWS::Lambda::Function
    Properties:
      Code: './lambdas/'
      Handler: "index.progressHandler"
      MemorySize: 128
      Role: !GetAtt "ProgressLambdaRole.Arn"
      Runtime: "nodejs8.10"
      Timeout: 3
      FunctionName: !Sub '${AWS::StackName}-ProgressLambda'
      ReservedConcurrentExecutions: 10
      Environment:
        Variables:
          PIPELINES_TABLE: !Ref PipelinesDb
          PROGRESS_TABLE: !Ref ProgressDb
          NEXTFLOW_CLUSTER_ARN: !GetAtt "HeadNodeCluster.Arn"
          NEXTFLOW_LOG_DIR: !Sub "s3://${FilesBucket}/logDir/"

  ProgressApiGatewayRootInvoke:
    Type: AWS::Lambda::Permission
    Properties:
      Action: "lambda:InvokeFunction"
      FunctionName: !GetAtt "ProgressLambda.Arn"
      Principal: "apigateway.amazonaws.com"
      SourceArn: !Sub "arn:aws:execute-api:${AWS::Region}:${AWS::AccountId}:${ProgressApi}/*/*/"

  ProgressApiGatewayProxyInvoke:
    Type: "AWS::Lambda::Permission"
    Properties:
      Action: "lambda:InvokeFunction"
      FunctionName: !GetAtt "ProgressLambda.Arn"
      Principal: "apigateway.amazonaws.com"
      SourceArn: !Sub "arn:aws:execute-api:${AWS::Region}:${AWS::AccountId}:${ProgressApi}/*/*/*"

  ProgressLambdaRole:
    Type: "AWS::IAM::Role"
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Action:
              - "sts:AssumeRole"
            Effect: "Allow"
            Principal:
              Service:
                - "lambda.amazonaws.com"
      Policies:
        - PolicyDocument:
            Version: "2012-10-17"
            Statement:
              - Action:
                  - "logs:CreateLogGroup"
                  - "logs:CreateLogStream"
                  - "logs:PutLogEvents"
                Effect: "Allow"
                Resource: !Sub "arn:aws:logs:${AWS::Region}:${AWS::AccountId}:log-group:/aws/lambda/${AWS::StackName}-ProgressLambda:*"
          PolicyName: "ProgressLambdaLogs"
        - PolicyDocument:
            Version: "2012-10-17"
            Statement:
              - Action:
                  - "dynamodb:*"
                Effect: "Allow"
                Resource:
                  - !GetAtt "PipelinesDb.Arn"
                  - !GetAtt "ProgressDb.Arn"
          PolicyName: "ProgressLambdaAccessDb"
        - PolicyDocument:
            Version: "2012-10-17"
            Statement:
              - Effect: Allow
                Action:
                - "s3:*"
                Resource: !GetAtt "FilesBucket.Arn"
              - Effect: Allow
                Action:
                - "s3:*"
                Resource: !Sub "${FilesBucket.Arn}/*"
          PolicyName: "ProgressLambdaAccessFilesBucket"

  ProgressLambdaLogGroup:
    Type: "AWS::Logs::LogGroup"
    Properties:
      LogGroupName: !Sub "/aws/lambda/${AWS::StackName}-ProgressLambda"
      RetentionInDays: 3

  ##############################
  # Create the Charges Lambda
  ##############################

  # Based on https://apimeister.com/2018/01/10/implementing-dynamodb-triggers-streams-using-cloudformation.html
  # by Jens Walter

  ChargesLambda:
    Type: AWS::Lambda::Function
    Properties:
      Code: './lambdas/'
      Handler: "index.chargesHandler"
      MemorySize: 128
      Role: !GetAtt "ChargesLambdaRole.Arn"
      Runtime: "nodejs8.10"
      Timeout: 3
      FunctionName: !Sub '${AWS::StackName}-ChargesLambda'
      ReservedConcurrentExecutions: 10
      Environment:
        Variables:
          USERS_TABLE: !Ref UsersDb
          PIPELINES_TABLE: !Ref PipelinesDb

  ChargesLambdaRole:
    Type: "AWS::IAM::Role"
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Action:
              - "sts:AssumeRole"
            Effect: "Allow"
            Principal:
              Service:
                - "lambda.amazonaws.com"
      Policies:
        - PolicyDocument:
            Version: "2012-10-17"
            Statement:
              - Action:
                  - "logs:CreateLogGroup"
                  - "logs:CreateLogStream"
                  - "logs:PutLogEvents"
                Effect: "Allow"
                Resource: !Sub "arn:aws:logs:${AWS::Region}:${AWS::AccountId}:log-group:/aws/lambda/${AWS::StackName}-ChargesLambda:*"
          PolicyName: "ChargesLambdaLogs"
        - PolicyDocument:
            Version: "2012-10-17"
            Statement:
              - Action:
                  - "dynamodb:*"
                Effect: "Allow"
                Resource:
                  - !GetAtt "UsersDb.Arn"
                  - !GetAtt "PipelinesDb.Arn"
                  - !GetAtt "PipelinesDb.StreamArn"
          PolicyName: "ChargesLambdaAccessDb"

  PipelinesToChargesStream:
    Type: AWS::Lambda::EventSourceMapping
    Properties:
      Enabled: True
      EventSourceArn: !GetAtt "PipelinesDb.StreamArn"
      FunctionName: !GetAtt "ChargesLambda.Arn"
      StartingPosition: LATEST

  ChargesLambdaLogGroup:
    Type: "AWS::Logs::LogGroup"
    Properties:
      LogGroupName: !Sub "/aws/lambda/${AWS::StackName}-ChargesLambda"
      RetentionInDays: 1

  ##############################
  # Create the Pipelines Lambda
  ##############################

  PipelineTimeoutEvents:
    Type: AWS::Events::Rule
    Properties:
      Description: "ScheduledRule"
      ScheduleExpression: "rate(10 minutes)"
      State: "ENABLED"
      Targets:
        - Arn: !GetAtt "PipelinesLambda.Arn"
          Id: "PipelinesLambdaTarget"

  PipelineTimeoutEventsPermission:
    Type: AWS::Lambda::Permission
    Properties:
      FunctionName: !Ref PipelinesLambda
      Action: "lambda:InvokeFunction"
      Principal: "events.amazonaws.com"
      SourceArn: !GetAtt "PipelineTimeoutEvents.Arn"

  # Based on https://apimeister.com/2018/01/10/implementing-dynamodb-triggers-streams-using-cloudformation.html
  # by Jens Walter

  PipelinesLambda:
    Type: AWS::Lambda::Function
    Properties:
      Code: './lambdas/'
      Handler: "index.pipelinesHandler"
      MemorySize: 1024
      Role: !GetAtt "PipelinesLambdaRole.Arn"
      Runtime: "nodejs8.10"
      Timeout: 60
      FunctionName: !Sub '${AWS::StackName}-PipelinesLambda'
      ReservedConcurrentExecutions: 10
      Environment:
        Variables:
          PIPELINES_TABLE: !Ref PipelinesDb
          HISTORY_TABLE: !Ref HistoryDb
          AWS_BATCH_QUEUE: !Select
            - 1
            - !Split
              - "/"
              - Ref: NextflowJobQueue
          NEXTFLOW_ROLE: !Ref HeadNodeRole
          NEXTFLOW_WORK_DIR: !Sub "s3://${FilesBucket}/workDir/"
          NEXTFLOW_LOG_DIR: !Sub "s3://${FilesBucket}/logDir/"
          NEXTFLOW_PROGRESS_URL: !Sub 'https://${ProgressApi}.execute-api.${AWS::Region}.amazonaws.com/default/'
          NEXTFLOW_CLUSTER: !Ref HeadNodeCluster
          NEXTFLOW_LOGS_GROUP: !Ref HeadNodeLogGroup
          NEXTFLOW_AWS_ACCESS_KEY_ID: !Ref HeadNodeUserKeys
          NEXTFLOW_AWS_SECRET_ACCESS_KEY: !GetAtt "HeadNodeUserKeys.SecretAccessKey"
          SUBNETS: !Join [",", [!Ref PublicSubnet1, !Ref PublicSubnet2]]

  PipelinesLambdaRole:
    Type: "AWS::IAM::Role"
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Action:
              - "sts:AssumeRole"
            Effect: "Allow"
            Principal:
              Service:
                - "lambda.amazonaws.com"
      Policies:
        - PolicyDocument:
            Version: "2012-10-17"
            Statement:
              - Action:
                  - "logs:CreateLogGroup"
                  - "logs:CreateLogStream"
                  - "logs:PutLogEvents"
                Effect: "Allow"
                Resource: !Sub "arn:aws:logs:${AWS::Region}:${AWS::AccountId}:log-group:/aws/lambda/${AWS::StackName}-PipelinesLambda:*"
          PolicyName: "PipelinesLambdaLogs"
        - PolicyDocument:
            Version: "2012-10-17"
            Statement:
              - Action:
                  - "dynamodb:*"
                Effect: "Allow"
                Resource:
                  - !GetAtt "HistoryDb.Arn"
                  - !GetAtt "PipelinesDb.Arn"
                  - !GetAtt "PipelinesDb.StreamArn"
          PolicyName: "PipelinesLambdaAccessDb"
      ManagedPolicyArns:
        - 'arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role'

  PipelinesToPipelinesStream:
    Type: AWS::Lambda::EventSourceMapping
    Properties:
      Enabled: True
      EventSourceArn: !GetAtt "PipelinesDb.StreamArn"
      FunctionName: !GetAtt "PipelinesLambda.Arn"
      StartingPosition: LATEST

  PipelinesLambdaLogGroup:
    Type: "AWS::Logs::LogGroup"
    Properties:
      LogGroupName: !Sub "/aws/lambda/${AWS::StackName}-PipelinesLambda"
      RetentionInDays: 1

  ##############################
  # Create the Frontend
  ##############################

  CloudFrontIdentity:
    Type: AWS::CloudFront::CloudFrontOriginAccessIdentity
    Condition: ShouldDeployFrontend
    Properties:
      CloudFrontOriginAccessIdentityConfig:
        Comment: Used to access Nextflow frontend

  # Based on template in https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/quickref-cloudfront.html
  # Retrieved 3 September 2019

  FrontendDistribution:
    Type: AWS::CloudFront::Distribution
    Condition: ShouldDeployFrontend
    Properties:
      DistributionConfig:
        DefaultCacheBehavior:
          Compress: true
          ForwardedValues:
            QueryString: false
          TargetOriginId: staticsite
          ViewerProtocolPolicy: redirect-to-https
          AllowedMethods:
          - GET
          - HEAD
          DefaultTTL: '100'
          MaxTTL: '600'
        CacheBehaviors:
        - TargetOriginId: frontend-api
          PathPattern: api/*
          AllowedMethods:
          - DELETE
          - GET
          - HEAD
          - OPTIONS
          - PATCH
          - POST
          - PUT
          Compress: true
          ForwardedValues:
            QueryString: 'true'
            Cookies:
              Forward: all
          ViewerProtocolPolicy: redirect-to-https
          DefaultTTL: 0
          MaxTTL: 0
        - TargetOriginId: frontend-api
          PathPattern: auth/*
          AllowedMethods:
          - DELETE
          - GET
          - HEAD
          - OPTIONS
          - PATCH
          - POST
          - PUT
          Compress: true
          ForwardedValues:
            QueryString: 'true'
            Cookies:
              Forward: all
          ViewerProtocolPolicy: redirect-to-https
          DefaultTTL: 0
          MaxTTL: 0
        DefaultRootObject: index.html
        CustomErrorResponses:
          - ErrorCachingMinTTL: 300
            ErrorCode: 403
            ResponseCode: 200
            ResponsePagePath: /index.html
        Enabled: true
        HttpVersion: http2
        Origins:
          - Id: staticsite
            DomainName: !Sub "${FrontendBucket}.s3-${AWS::Region}.amazonaws.com"
            S3OriginConfig:
              OriginAccessIdentity: !Sub "origin-access-identity/cloudfront/${CloudFrontIdentity}"
            OriginPath: !Ref FrontendStaticPrefix
          - Id: frontend-api
            DomainName: !Sub ${FrontendApi}.execute-api.${AWS::Region}.amazonaws.com
            CustomOriginConfig:
              HTTPSPort: '443'
              OriginProtocolPolicy: https-only
            OriginPath: /default
        PriceClass: PriceClass_100

  FrontendBucket:
    Type: AWS::S3::Bucket
    Condition: ShouldDeployFrontend
    DeletionPolicy: Delete

  BucketPolicy:
    Type: AWS::S3::BucketPolicy
    Condition: ShouldDeployFrontend
    Properties:
      PolicyDocument:
        Id: ReadFrontendBucket
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
                CanonicalUser: !GetAtt "CloudFrontIdentity.S3CanonicalUserId"
            Action: 's3:GetObject'
            Resource: !Sub 'arn:aws:s3:::${FrontendBucket}/*'
      Bucket: !Ref FrontendBucket

  ##############################
  # Create the session store
  ##############################
  SessionsDb:
    Type: AWS::DynamoDB::Table
    Condition: ShouldDeployFrontend
    Properties:
      AttributeDefinitions:
        - AttributeName: id
          AttributeType: S
      BillingMode: PAY_PER_REQUEST
      KeySchema:
        - AttributeName: id
          KeyType: HASH
      TimeToLiveSpecification:
        AttributeName: expires
        Enabled: true

  ##############################
  # Create the Frontend API
  ##############################

  # Inspired by https://gist.github.com/magnetikonline/c314952045eee8e8375b82bc7ec68e88
  # by Peter Mescalchin

  FrontendApi:
    Type: "AWS::ApiGateway::RestApi"
    Condition: ShouldDeployFrontend
    Properties:
      Name: !Sub 'FrontendApi-${AWS::StackName}'
      Description: !Sub 'Collects progress reports from ${AWS::StackName}'

  FrontendApiRootMethod:
    Type: AWS::ApiGateway::Method
    Condition: ShouldDeployFrontend
    Properties:
      RestApiId: !Ref FrontendApi
      ResourceId: !GetAtt "FrontendApi.RootResourceId"
      HttpMethod: ANY
      AuthorizationType: NONE
      Integration:
        Type: AWS_PROXY
        IntegrationHttpMethod: POST
        Uri: !Sub arn:aws:apigateway:${AWS::Region}:lambda:path/2015-03-31/functions/${FrontendLambda.Arn}/invocations

  FrontendApiProxyResource:
    Type: AWS::ApiGateway::Resource
    Condition: ShouldDeployFrontend
    Properties:
      RestApiId: !Ref FrontendApi
      ParentId: !GetAtt "FrontendApi.RootResourceId"
      PathPart: '{proxy+}'

  FrontendApiProxyMethod:
    Type: AWS::ApiGateway::Method
    Condition: ShouldDeployFrontend
    Properties:
      RestApiId: !Ref FrontendApi
      ResourceId: !Ref FrontendApiProxyResource
      HttpMethod: ANY
      AuthorizationType: NONE
      Integration:
        Type: AWS_PROXY
        IntegrationHttpMethod: POST
        Uri: !Sub arn:aws:apigateway:${AWS::Region}:lambda:path/2015-03-31/functions/${FrontendLambda.Arn}/invocations

  FrontendApiDeployment:
    Type: AWS::ApiGateway::Deployment
    Condition: ShouldDeployFrontend
    DependsOn:
      - FrontendApiRootMethod
      - FrontendApiProxyMethod
    Properties:
      RestApiId: !Ref FrontendApi
      StageName: default

  FrontendLambda:
    Type: AWS::Lambda::Function
    Condition: ShouldDeployFrontend
    Properties:
      Code: './frontend/lambda/'
      Handler: "index.handler"
      MemorySize: 1024
      Role: !GetAtt "FrontendLambdaRole.Arn"
      Runtime: "nodejs8.10"
      Timeout: 3
      FunctionName: !Sub '${AWS::StackName}-FrontendLambda'
      ReservedConcurrentExecutions: 10
      Environment:
        Variables:
          TABLE_USERS: !Ref UsersDb
          TABLE_JOBS: !Ref PipelinesDb
          NODE_ENV: production
          SESSION_SECRET: !Ref SessionSecret
          SESSION_TABLE: !Ref SessionsDb

  FrontendApiGatewayRootInvoke:
    Type: AWS::Lambda::Permission
    Condition: ShouldDeployFrontend
    Properties:
      Action: "lambda:InvokeFunction"
      FunctionName: !GetAtt "FrontendLambda.Arn"
      Principal: "apigateway.amazonaws.com"
      SourceArn: !Sub "arn:aws:execute-api:${AWS::Region}:${AWS::AccountId}:${FrontendApi}/*/*/"

  FrontendApiGatewayProxyInvoke:
    Type: "AWS::Lambda::Permission"
    Condition: ShouldDeployFrontend
    Properties:
      Action: "lambda:InvokeFunction"
      FunctionName: !GetAtt "FrontendLambda.Arn"
      Principal: "apigateway.amazonaws.com"
      SourceArn: !Sub "arn:aws:execute-api:${AWS::Region}:${AWS::AccountId}:${FrontendApi}/*/*/*"

  FrontendLambdaRole:
    Type: "AWS::IAM::Role"
    Condition: ShouldDeployFrontend
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Action:
              - "sts:AssumeRole"
            Effect: "Allow"
            Principal:
              Service:
                - "lambda.amazonaws.com"
      Policies:
        - PolicyDocument:
            Version: "2012-10-17"
            Statement:
              - Action:
                  - "logs:CreateLogGroup"
                  - "logs:CreateLogStream"
                  - "logs:PutLogEvents"
                Effect: "Allow"
                Resource: !Sub "arn:aws:logs:${AWS::Region}:${AWS::AccountId}:log-group:/aws/lambda/${AWS::StackName}-FrontendLambda:*"
          PolicyName: "FrontendLambdaLogs"
        - PolicyDocument:
            Version: "2012-10-17"
            Statement:
              - Action:
                  - "dynamodb:*"
                Effect: "Allow"
                Resource:
                  - !GetAtt "UsersDb.Arn"
                  - !GetAtt "PipelinesDb.Arn"
                  - !Sub "${PipelinesDb.Arn}/*"
                  - !GetAtt "SessionsDb.Arn"
          PolicyName: "FrontendLambdaAccessDb"

  FrontendLambdaLogGroup:
    Type: "AWS::Logs::LogGroup"
    Condition: ShouldDeployFrontend
    Properties:
      LogGroupName: !Sub "/aws/lambda/${AWS::StackName}-FrontendLambda"
      RetentionInDays: 3

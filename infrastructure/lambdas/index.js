const { errorLogger } = require('./utils')

const defaultResponse = {
  body: "There was an unhandled error which has been logged",
  statusCode: 500
}
const preemptTimeout = 300

module.exports = {
  chargesHandler: errorLogger(() => require('./charges').handler, { defaultResponse, preemptTimeout }),
  pipelinesHandler: errorLogger(() => require('./pipelines').handler, { defaultResponse, preemptTimeout }),
  progressHandler: errorLogger(() => require('./progress').handler, { defaultResponse, preemptTimeout })
}
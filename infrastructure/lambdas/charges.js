const AWS = require("aws-sdk");

const AWS_REGION = process.env.AWS_REGION || process.env.AWS_DEFAULT_REGION;
AWS.config.update({
  region: AWS_REGION
});

const { parseDynamoOutput, transactWriteItems, jitter } = require('./utils')

const docClient = new AWS.DynamoDB.DocumentClient();

const PIPELINES_TABLE = process.env.PIPELINES_TABLE;
const USERS_TABLE = process.env.USERS_TABLE;
const MAX_RETRIES = 10;

function environmentVariablesOk() {
  switch (undefined) {
    case PIPELINES_TABLE:
      console.log({ error: "PIPELINES_TABLE is undefined" })
      return false;
    case USERS_TABLE:
      console.log({ error: "USERS_TABLE is undefined" })
      return false;
    default:
      return true
  }
}

// STATUSES
const READY = "READY"
const PAID = "PAID"
const OUT_OF_CREDITS = "OUT_OF_CREDITS"
const FAILED = "FAILED"

async function getUser(user) {
  const { Item } = await docClient.get({
    TableName: USERS_TABLE,
    Key: { user }
  }).promise()
  return Item
}

async function setPipelineStatus(status, pipeline, retries=MAX_RETRIES) {
  try {
    return await docClient.update({
      TableName: PIPELINES_TABLE,
      Key: { pipeline },
      UpdateExpression: "set charges.#status = :newStatus, charges.#updated = :now",
      ConditionExpression: 'charges.#status = :ready',
      ExpressionAttributeValues: {
        ":newStatus": status,
        ":ready": READY,
        ":now": Math.floor(new Date().getTime() / 1000)
      },
      ExpressionAttributeNames: {
        "#status": "status",
        "#updated": "updated"
      }
    }).promise()
  } catch (err) {
    console.log({ err: err.code, retries, retry: "setPipelineStatus", pipeline })
    if (retries <= 0) return
    else if (err.code && err.code === 'ConditionalCheckFailedException') return
    else if (err.code && err.code === 'TransactionConflictException') {
      await jitter(200)
      return setPipelineStatus(status, pipeline, retries-1)
    }
    return
  }
}

async function updateUserCredits(user, retries=MAX_RETRIES) {
  const { credits } = (await getUser(user)) || {};
  if (credits === undefined) return null
  const { balance=0, rate=0, lastDebited, maxBalance: _maxBalance } = credits;
  const maxBalance = _maxBalance !== undefined ? _maxBalance : rate * 730 * 60 * 60;
  const now = (new Date()).getTime() / 1000;

  const newBalance = Math.min(maxBalance, (now - lastDebited) * rate + balance);
  if ((newBalance - balance) < 1) {
    console.log({ debug: "not a big diff", user, retries, balance, newBalance, lastDebited })
    return balance
  }
  try {
    await docClient.update({
      TableName: USERS_TABLE,
      Key: { user },
      UpdateExpression: "set credits.balance = :newBalance, credits.lastDebited = :now",
      ConditionExpression: 'credits.lastDebited = :lastDebited',
      ExpressionAttributeValues: {
        ":newBalance": newBalance,
        ":lastDebited": lastDebited,
        ":now": now
      }
    }).promise()
    return newBalance
  } catch (err) {
    console.log({ retries, retry: "updateUserCredits", user })
    if (retries <= 0) return balance
    if (err.code && (err.code === 'ConditionalCheckFailedException' || err.code === 'TransactionConflictException')) {
      await jitter(200)
      return updateUserCredits(user, retries-1)
    }
    return balance
  }
}

async function attemptGlobalCharge(cost, retries=MAX_RETRIES) {
  try {
    await docClient.update({
      TableName: USERS_TABLE,
      Key: { user: "__GLOBAL" },
      ConditionExpression: 'credits.balance >= :cost',
      UpdateExpression: 'set credits.balance = credits.balance - :cost',
      ExpressionAttributeValues: {
        ':cost': cost,
      },
      ReturnValues:"UPDATED_NEW"
    }).promise()
    return { status: PAID }
  } catch (err) {
    console.log({ err: err.code, retries, retry: "attemptGlobalCharge", cost })
    if (retries <= 0) return { status: FAILED }
    if (err.code && err.code === 'ConditionalCheckFailedException') {
      const credits = await updateUserCredits("__GLOBAL")
      if (credits < cost) return { status: OUT_OF_CREDITS }
      return attemptGlobalCharge(cost, retries-1)
    } else if (err.code && err.code === 'TransactionConflict') {
      await jitter(200)
      return attemptGlobalCharge(cost, retries-1)
    }
    return { status: FAILED }
  }
}

async function attemptUserCharge(user, cost, pipeline, retries=MAX_RETRIES) {
  try {
    await transactWriteItems({
      TransactItems: [
        {
          Update: {
            TableName: USERS_TABLE,
            Key: { user: { S: user }},
            ConditionExpression: 'credits.balance >= :cost',
            UpdateExpression: 'set credits.balance = credits.balance - :cost',
            ExpressionAttributeValues: {
              ':cost': { N: cost.toString() },
            },
            ReturnValuesOnConditionCheckFailure: "ALL_OLD"
          }
        },
        {
          Update: {
            TableName: PIPELINES_TABLE,
            Key: { pipeline: { S: pipeline } },
            ConditionExpression: 'charges.#status = :ready OR charges.#status = :outOfCredits',
            UpdateExpression: 'set charges.#status = :paid, charges.#updated = :now',
            ExpressionAttributeValues: {
              ':ready': { S: READY },
              ':paid': { S: PAID },
              ":outOfCredits": { S: OUT_OF_CREDITS },
              ":now": { N: Math.floor(new Date().getTime() / 1000).toString() }
            },
            ExpressionAttributeNames: {
              "#status": "status",
              "#updated": "updated"
            },
            ReturnValuesOnConditionCheckFailure: "ALL_OLD"
          }
        }
      ]
    })
    return { status: PAID }
  } catch (err) {
    const reasons = (err.CancellationReasons || []).map(_ => _.Code);
    console.log({ retries, retry: "attemptUserCharge", user, cost, pipeline })
    if (reasons[1] && reasons[1] === "ConditionalCheckFailed") {
      return { status: PAID }
    }

    if (reasons[0] === "ConditionalCheckFailed") {
      const credits = await updateUserCredits(user)
      if (credits < cost) return { status: OUT_OF_CREDITS }
    }

    if (reasons.length > 0 && retries>0) {
      await jitter(200);
      return attemptUserCharge(user, cost, pipeline, retries-1)
    }

    console.log({retry: "attemptUserCharge", user, cost, pipeline, err})
    return { status: FAILED }
  }
}

async function attemptCharge(pipeline, user, cost) {
  const { status: globalStatus } = await attemptGlobalCharge(cost);
  switch (globalStatus) {
    case PAID:
      break;
    case OUT_OF_CREDITS:
      await setPipelineStatus(OUT_OF_CREDITS, pipeline)
      console.log({ error: `global limit exceeded for ${pipeline}` })
      return { status: OUT_OF_CREDITS };
    case FAILED:
    default:
      console.log({ error: `error updating global limit for ${pipeline}` })
      return { status: FAILED };
  }

  const { status: userStatus } = await attemptUserCharge(user, cost, pipeline)
  switch (userStatus) {
    case PAID:
      break;
    case OUT_OF_CREDITS:
      await setPipelineStatus(OUT_OF_CREDITS, pipeline)
      console.log({ error: `user ${user} limit exceeded for ${pipeline}` })
    case FAILED:
    default:
      const { status: revertStatus } = await attemptGlobalCharge(-cost);
      if (revertStatus === PAID) console.log({ debug: `reverted global charge for ${pipeline}` })
      else console.log({ error: `couldn't revert global charge for ${pipeline}` })
      return { status: userStatus }
  }

  console.log({ debug: `Charged ${user} ${cost} for ${pipeline}` })
  return { status: PAID }
}

async function update(NewImage, OldImage) {
  const data = parseDynamoOutput({ M: NewImage })
  const { cost, status } = data.charges || {};
  const { pipeline, user } = data;
  if (status !== READY || cost === undefined) return
  if (cost <= 0) return setPipelineStatus(PAID, pipeline)
  return attemptCharge(pipeline, user, cost);
}

exports.handler = async (event) => {
  if (!environmentVariablesOk()) return
  const records = event.Records || [];
  const dbRecords = records.filter(r => r.eventSource === "aws:dynamodb")
  for (const r of dbRecords) {
    const {NewImage, OldImage} = r.dynamodb;
    await update(NewImage, OldImage)
  }
};

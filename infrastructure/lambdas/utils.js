const AWS = require("aws-sdk");

const AWS_REGION = process.env.AWS_REGION || process.env.AWS_DEFAULT_REGION;
AWS.config.update({
  region: AWS_REGION
});
const dynamodb = new AWS.DynamoDB()
const s3 = new AWS.S3();

module.exports.forObjects = async function ({ Bucket, Prefix }, fn) {
  async function more(ContinuationToken) {
    const resp = await s3.listObjectsV2({
      Bucket,
      Prefix,
      ContinuationToken,
      MaxKeys: 800
    }).promise();
    const { IsTruncated, Contents=[], NextContinuationToken } = resp;
    const keys = Contents.map(_ => _.Key);

    if (keys.length === 0) return 0;

    await fn(keys)

    if (IsTruncated) return keys.length + (await more(NextContinuationToken))
    return keys.length
  }

  return await more()
}

function parseDynamoOutput(data) {
  const { L, S, M, N, BOOL } = data;
  if (L) return L.map(_ => parseDynamoOutput(_))
  if (M) {
    const output = {};
    for (const key of Object.keys(M)) output[key] = parseDynamoOutput(M[key])
    return output
  }
  if (S) return S;
  if (N) return Number(N);
  if (BOOL) return BOOL;
  throw Error(`Unhandled type '${Object.keys(data)[0]}'`)
}
module.exports.parseDynamoOutput = parseDynamoOutput;

module.exports.transactWriteItems = function (params) {
  return new Promise((resolve, reject) => {
    dynamodb.transactWriteItems(params).
      on('error', error => reject(error)).
      on('extractError', resp => reject(JSON.parse(resp.httpResponse.body.toString()))).
      on('success', resp => resolve(resp.data)).
      send()
  })
}

module.exports.jitter = function (t) {
  const delay = t * 0.1 + Math.random() * t * 0.9;
  return new Promise(r => setTimeout(() => r(delay), delay))
}

function timeout(context, preemptTimeout) {
  const t = Math.max(0, context.getRemainingTimeInMillis() - preemptTimeout);
  return new Promise((_, reject) => {
    setTimeout(() => reject(new Error(`Timed out after ${t}ms`)), t)
  })
}

function unhandled() {
  return new Promise((resolve, reject) => {
    process.on('unhandledRejection', (error, p) => {
      reject(error)
    })
  })
}

function flush() {
  return new Promise(resolve => {
    process.stderr.write('', () => {
      process.stdout.write('', () => resolve())
    })
  })
}

function log(...args) {
  console.log(...args);
  return flush();
}

module.exports.errorLogger = function (fetchHandler, { preemptTimeout, defaultResponse={} }) {
  let handler
  try {
    handler = fetchHandler();
  } catch (error) {
    return async (event, context) => {
      await log('problem loading handler', error)
      context.callbackWaitsForEmptyEventLoop = false;
      return defaultResponse;
    }
  }

  return async (event, context) => {
    try {
      const p = [
        handler(event, context),
        unhandled()
      ]
      if (preemptTimeout) p.push(timeout(context, preemptTimeout))
      const r = await Promise.race(p);
      await flush()
      context.callbackWaitsForEmptyEventLoop = false;
      return r;
    } catch (error) {
      await log('unhandled', error);
      context.callbackWaitsForEmptyEventLoop = false;
      return defaultResponse;
    }
  };
}

const AWS = require("aws-sdk");
const crypto = require("crypto");
const { URL } = require("url");

const AWS_REGION = process.env.AWS_REGION || process.env.AWS_DEFAULT_REGION;
AWS.config.update({
  region: AWS_REGION
});

const { parseDynamoOutput, jitter, forObjects } = require('./utils')

const ecs = new AWS.ECS();
const docClient = new AWS.DynamoDB.DocumentClient();

// Statuses
const PAID = "PAID"
const PENDING = "PENDING"
const STARTING = "STARTING"
const READY = "READY"
const FAILED = "FAILED"
const SUCCESS = "SUCCESS"

const {
  PIPELINES_TABLE,
  HISTORY_TABLE,
  AWS_BATCH_QUEUE,
  NEXTFLOW_ROLE,
  NEXTFLOW_WORK_DIR,
  NEXTFLOW_PROGRESS_URL,
  NEXTFLOW_CLUSTER,
  NEXTFLOW_LOGS_GROUP,
  NEXTFLOW_AWS_ACCESS_KEY_ID,
  NEXTFLOW_AWS_SECRET_ACCESS_KEY,
  SUBNETS
} = process.env
const MAX_RETRIES = 10;

function environmentVariablesOk() {
  var allOk=true
  if (PIPELINES_TABLE === undefined) { console.log({ error: 'PIPELINES_TABLE is undefined' }); allOK=false }
  if (HISTORY_TABLE === undefined) { console.log({ error: 'HISTORY_TABLE is undefined' }); allOK=false }
  if (AWS_BATCH_QUEUE === undefined) { console.log({ error: 'AWS_BATCH_QUEUE is undefined' }); allOK=false }
  if (NEXTFLOW_ROLE === undefined) { console.log({ error: 'NEXTFLOW_ROLE is undefined' }); allOK=false }
  if (NEXTFLOW_WORK_DIR === undefined) { console.log({ error: 'NEXTFLOW_WORK_DIR is undefined' }); allOK=false }
  if (NEXTFLOW_PROGRESS_URL === undefined) { console.log({ error: 'NEXTFLOW_PROGRESS_URL is undefined' }); allOK=false }
  if (NEXTFLOW_CLUSTER === undefined) { console.log({ error: 'NEXTFLOW_CLUSTER is undefined' }); allOK=false }
  if (NEXTFLOW_LOGS_GROUP === undefined) { console.log({ error: 'NEXTFLOW_LOGS_GROUP is undefined' }); allOK=false }
  if (NEXTFLOW_AWS_ACCESS_KEY_ID === undefined) { console.log({ error: 'NEXTFLOW_AWS_ACCESS_KEY_ID is undefined' }); allOK=false }
  if (NEXTFLOW_AWS_SECRET_ACCESS_KEY === undefined) { console.log({ error: 'NEXTFLOW_AWS_SECRET_ACCESS_KEY is undefined' }); allOK=false }
  if (SUBNETS === undefined) { console.log({ error: 'SUBNETS is undefined' }); allOK=false }
  return allOk
}

async function markStarting(pipeline, timestamp, retries=MAX_RETRIES) {
  try {
    await docClient.update({
      TableName: PIPELINES_TABLE,
      Key: { pipeline },
      UpdateExpression: "set #status = :starting, started = :timestamp",
      ConditionExpression: '#status = :pending',
      ExpressionAttributeValues: {
        ":starting": STARTING,
        ":pending": PENDING,
        ":timestamp": timestamp.getTime() / 1000
      },
      ExpressionAttributeNames: {
        "#status": "status"
      }
    }).promise()
    return true
  } catch (err) {
    console.log({ retries, retry: "markStarting", pipeline })
    if (retries <= 0) return
    if (err.code && err.code === 'ConditionalCheckFailedException') return false // Pipeline in the wrong state
    if (err.code && err.code === 'TransactionConflictException') {
      await jitter(200)
      return markStarting(pipeline, timestamp, retries-1)
    }
    return false
  }
}

async function rollbackStarting(pipeline, timestamp, retries=MAX_RETRIES) {
  try {
    await docClient.update({
      TableName: PIPELINES_TABLE,
      Key: { pipeline },
      UpdateExpression: "set #status = :failed",
      ConditionExpression: '#status = :starting AND started = :timestamp',
      ExpressionAttributeValues: {
        ":starting": STARTING,
        ":failed": FAILED,
        ":timestamp": timestamp.getTime() / 1000
      },
      ExpressionAttributeNames: {
        "#status": "status"
      }
    }).promise()
    return true
  } catch (err) {
    console.log({ retries, retry: "rollbackStarting", pipeline })
    if (retries <= 0) return
    if (err.code && err.code === 'ConditionalCheckFailedException') return false // Pipeline in the wrong state
    if (err.code && err.code === 'TransactionConflictException') {
      await jitter(200)
      return rollbackStarting(pipeline, timestamp, retries-1)
    }
    return false
  }
}

function parse(image) {
  const VERSION=6; // Used to invalidate old job definitions if required
  const split = image.split('/')
  const name_version = split[split.length-1]
  const name = name_version.split(':')[0]
  const version = name_version.split(':')[1] || "latest"
  const hash = crypto.createHash('sha1');
  hash.update(`${image}/${VERSION}/${NEXTFLOW_ROLE}`);
  return { name, version, hash: hash.digest('hex').slice(0, 8) }
}

async function findTaskDefinition(taskFamily) {
  const response = await ecs.listTaskDefinitions({
    familyPrefix: taskFamily,
    sort: "DESC",
    maxResults: 1,
    status: "ACTIVE"
  }).promise();
  const { taskDefinitionArns=[] } = response;
  if (taskDefinitionArns.length > 0) return taskDefinitionArns[0]
  return undefined
}

async function createTaskDefinition(taskFamily, image) {
  const params = {
    family: taskFamily,
    taskRoleArn: NEXTFLOW_ROLE,
    executionRoleArn: NEXTFLOW_ROLE,
    networkMode: 'awsvpc',
    containerDefinitions: [{
      entryPoint: [ "bash", "-o", "pipefail", "-c" ],
      image,
      name: 'nextflow',
      logConfiguration: {
        logDriver: 'awslogs',
        options: {
          "awslogs-region": AWS_REGION,
          "awslogs-group": NEXTFLOW_LOGS_GROUP,
          "awslogs-stream-prefix": taskFamily
        }
      }
    }],
    requiresCompatibilities: [ "FARGATE" ],
    cpu: '256',
    memory: '512',
    tags: [
      { key: 'image', value: image }
    ]
  };
  const resp = await ecs.registerTaskDefinition(params).promise();
  const { taskDefinition={} } = resp;
  const { taskDefinitionArn } = taskDefinition;
  return taskDefinitionArn
}

function formatMemory(pipelineMemory) {
  if (pipelineMemory > 8 * 1024) return "8G";
  if (pipelineMemory > 1024) return `${Math.ceil(pipelineMemory * 10 / 1024) / 10}G`;
  return `${Math.ceil(pipelineMemory)}M`;
}

function getWeblogUrl(pipeline) {
  if (NEXTFLOW_PROGRESS_URL === undefined) return undefined;
  const url = new URL(`${NEXTFLOW_PROGRESS_URL}/nextflow/${pipeline}`)
  url.pathname = url.pathname.replace(/\/+/g, '/');
  return url.href;
}

function getWorkDir(pipeline) {
  if (NEXTFLOW_WORK_DIR === undefined || !NEXTFLOW_WORK_DIR.startsWith('s3://')) throw Error(`NEXTFLOW_WORK_DIR should be like 's3://bucket/path' not ${NEXTFLOW_WORK_DIR}`);
  const dir = new URL(`${NEXTFLOW_WORK_DIR}/${pipeline}`)
  dir.pathname = dir.pathname.replace(/\/+/g, '/')
  return dir.href;
}

const nextflowConfig = `\
executor.awscli = '/home/ec2-user/miniconda/bin/aws'

aws {
    accessKey = "$__NF_RUNNER_AWS_ACCESS_KEY_ID"
    secretKey = "$__NF_RUNNER_AWS_SECRET_ACCESS_KEY"
    region = "$__NF_RUNNER_AWS_DEFAULT_REGION"
}

process {
  executor = 'awsbatch'
  queue = "$__NF_RUNNER_AWS_BATCH_QUEUE"
  memory = "$__NF_RUNNER_MEMORY"
  container = "$__NF_RUNNER_CONTAINER"
}
`

async function addPipelineTask({ pipeline, args=[], pipelineContainer, pipelineDependencies, pipelineMemory, timeout }) {
  const { name, hash: containerHash } = parse(pipelineContainer)
  const taskDefinitionFamily = `nf-run-${name}-${containerHash}`

  const weblogUrl = getWeblogUrl(pipeline);
  const command = [
    "nextflow",
    "run",
    ...args,
    "-c",
    "/tmp/__nf_runner.config",
    "-work-dir",
    getWorkDir(pipeline)
  ]
  if (weblogUrl) command.push("-with-weblog", weblogUrl);

  const runnerTimeout = timeout > 0 ? String(Math.ceil(new Date().getTime() / 1000 + timeout)) : "0"

  const environment = [
    { name: "__NF_RUNNER_CONFIG", value: Buffer(nextflowConfig).toString('base64') },
    { name: "__NF_RUNNER_AWS_BATCH_QUEUE", value: AWS_BATCH_QUEUE },
    { name: "__NF_RUNNER_MEMORY", value: formatMemory(pipelineMemory) },
    { name: "__NF_RUNNER_CONTAINER", value: pipelineDependencies },
    { name: "__NF_RUNNER_AWS_ACCESS_KEY_ID", value: NEXTFLOW_AWS_ACCESS_KEY_ID },
    { name: "__NF_RUNNER_AWS_SECRET_ACCESS_KEY", value: NEXTFLOW_AWS_SECRET_ACCESS_KEY },
    { name: "__NF_RUNNER_AWS_DEFAULT_REGION", value: AWS_REGION },
    { name: "__NF_RUNNER_PIPELINE", value: pipeline },
    { name: "__NF_RUNNER_TIMEOUT", value: runnerTimeout }
  ]

  var taskDefinitionArn = await findTaskDefinition(taskDefinitionFamily)
  if (!taskDefinitionArn) {
    taskDefinitionArn = await createTaskDefinition(taskDefinitionFamily, pipelineContainer)
  }

  commandString = `function cat_log { ret=$?; sleep 1; echo "Nextflow exited with status $ret"; echo "=== NEXTFLOW LOGS ==="; cat -n .nextflow.log||true; exit $ret; }; trap cat_log EXIT; echo $__NF_RUNNER_CONFIG | base64 -d > /tmp/__nf_runner.config && ${command.join(" ")}`
  
  const params = {
    taskDefinition: taskDefinitionArn,
    cluster: NEXTFLOW_CLUSTER,
    count: 1,
    launchType: "FARGATE",
    networkConfiguration: {
      awsvpcConfiguration: {
        assignPublicIp: "ENABLED",
        subnets: SUBNETS.split(','),
      }
    },
    overrides: {
      containerOverrides: [
        {
          command: [ commandString ],
          environment,
          name: 'nextflow',
        },
      ]
    },
    startedBy: pipeline,
  };

  const resp = await ecs.runTask(params).promise()
  console.log({ message: `started ${pipeline}`, pipeline, ...resp })
}

function parseDetailsFromTask(task) {
  const { taskArn, overrides={} } = task;
  const { containerOverrides=[] } = overrides;

  // The timeout is set as an environment variable
  const timeouts = containerOverrides.map(({ environment=[] }) =>
    environment
      .filter(_ => _.name === '__NF_RUNNER_TIMEOUT')
      .map(_ => _.value)[0]
  ).filter(_ => _ !== undefined);
  const timeout = Math.min(...timeouts);

  // The name of the pipeline is set as an environment variable
  const pipelines = containerOverrides.map(({ environment=[] }) =>
    environment
      .filter(_ => _.name === '__NF_RUNNER_PIPELINE')
      .map(_ => _.value)[0]
  ).filter(_ => _ !== undefined);
  const pipeline = pipelines[0]

  return { taskArn, timeout, pipeline }
}

async function timeoutTasks() {
  async function next(nextToken) {
    const r = await ecs.listTasks({
      cluster: NEXTFLOW_CLUSTER,
      desiredStatus: "RUNNING",
      launchType: "FARGATE",
      nextToken,
    }).promise()
    const { taskArns=[], nextToken: token } = r;
    if (taskArns.length === 0) return [];
    const taskIds = taskArns.map(t => t.split('/').pop(-1))
    const details = await ecs.describeTasks({ cluster: NEXTFLOW_CLUSTER, tasks: taskIds }).promise();
    const { tasks } = details;
    const timeouts = tasks.map(parseDetailsFromTask)
    if (nextToken) return [...timeouts, ...(await next(token))]
    return timeouts
  }

  const now = Math.floor(new Date().getTime() / 1000)
  const timeouts = await next();
  const overdue = timeouts.filter(_ => _.timeout < now && _.timeout > 0);
  console.log(`${overdue.length} pipelines have exceeded their timeout and will be terminated`)
  for (const { taskArn: task, timeout, pipeline } of overdue) {
    const reason = `Timeout: Pipeline ${pipeline} should have completed by ${new Date(timeout * 1000)} (${Math.floor(new Date().getTime() / 1000) - timeout} seconds ago)`
    await ecs.stopTask({
      task,
      cluster: NEXTFLOW_CLUSTER,
      reason
    }).promise()
    console.log(reason)
  }
}

async function addToHistory(pipeline, hash, retries=MAX_RETRIES) {
  try {
    await docClient.update({
      TableName: HISTORY_TABLE,
      Key: { hash },
      UpdateExpression: "ADD pipelines :pipeline",
      ExpressionAttributeValues: {
        ":pipeline": pipeline
      }
    }).promise()
    return true
  } catch (err) {
    console.log({ retries, retry: "addToHistory", pipeline })
    if (retries <= 0) return false
    if (err.code && err.code === 'TransactionConflictException') {
      await jitter(200)
      return addToHistory(pipeline, hash, retries-1)
    }
    return false
  }
}

function validate(inputs) {
  const { args, hash = "unset", pipelineContainer, pipelineDependencies: _pipelineDependencies, memory: pipelineMemory=1024, timeout=3600 } = inputs;
  const pipelineDependencies = _pipelineDependencies === undefined ? pipelineContainer : _pipelineDependencies
  switch (undefined) {
    case args:
      return { error: "args is undefined" }
    case hash:
      // TODO: don't set a default value above
      return { error: "hash is undefined" }
    case pipelineContainer:
      return { error: "pipelineContainer is undefined" }
  }
  if (!Array.isArray(args)) {
    return { error: "args should be an array" }
  }
  return { args, hash, pipelineContainer, pipelineDependencies, pipelineMemory, timeout }
}

async function update(NewImage, OldImage) {
  const data = parseDynamoOutput({ M: NewImage || {} })
  const oldData = parseDynamoOutput({ M: OldImage || {} })
  const { user, pipeline, status, inputs, charges } = data;
  const { status: oldStatus } = oldData;
  if (status !== oldStatus && (status === SUCCESS || status === FAILED)) {
    // TODO backup the nextflow logs to S3
    return
  }
  if (status !== PENDING) return;
  if (inputs.status !== READY) return;
  if (charges && charges.status && charges.status !== PAID) return;

  const { error, args, hash, pipelineContainer, pipelineDependencies, pipelineMemory, timeout } = validate(inputs);
  if (error !== undefined) {
    console.log({ error, user, pipeline })
    return
  }

  // Do something if a pipeline with this hash has already run

  const timestamp = new Date()
  if (!await markStarting(pipeline, timestamp)) {
    console.log({ error: "could not mark pipeline as starting", user, pipeline })
    return;
  }
  try {
    await addPipelineTask({ pipeline, args, pipelineContainer, pipelineDependencies, pipelineMemory, timeout })
    await addToHistory(pipeline, hash);
  } catch (err) {
    console.log(err);
    await rollbackStarting(pipeline, timestamp)
    return
  }

  console.log({ debug: `Running ${pipeline} in ${pipelineContainer} for ${user}`})
}

exports.handler = async (event) => {
  if (!environmentVariablesOk()) return
  if (event.source === "aws.events") {
    const { resources=[] } = event;
    const timeoutResources = resources.filter(_ => _.includes('PipelineTimeoutEvents'))
    if (timeoutResources.length > 0) return await timeoutTasks();
  }
  const records = event.Records || [];
  const dbRecords = records.filter(r => r.eventSource === "aws:dynamodb")
  for (const r of dbRecords) {
    const {NewImage, OldImage} = r.dynamodb;
    await update(NewImage, OldImage)
  }
};
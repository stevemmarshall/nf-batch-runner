import Cookies from 'universal-cookie';
const cookies = new Cookies();

export default ({ store, redirect }) => {
  const isAuthenticated = cookies.get('user');
  if (typeof isAuthenticated === 'undefined') {
    return redirect('/');
  }
};

import { getJobs } from './helpers/api';

export const state = () => ({
  data: [],
  loading: false,
  limit: {
    value: 20,
    multiplier: 1,
  },
  polling: {
    isActive: true,
  },
});

export const getters = {
  names: () => [],
};

export const mutations = {
  setData(state, value) {
    state.data = value;
  },
  setLoading(state, value) {
    state.loading = value;
  },
  raiseLimit(state) {
    state.limit.multiplier += 1;
  },
  setPollingIsActive(state, value) {
    state.polling.isActive = value;
  },
};

export const actions = {
  // history
  async getJobs({ commit, state }) {
    commit('setLoading', true);
    try {
      const limit = (state.limit.value * state.limit.multiplier);
      const { data = [] } = await getJobs({ limit });
      if (data.length <= 0) {
        commit('setPollingIsActive', false);
      }
      commit('setData', data);
    } catch (exception) {
      commit('setPollingIsActive', false);
    }
    commit('setLoading', false);
  },
};

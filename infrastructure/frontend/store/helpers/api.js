import dev from '~/static/dev.json';
const config = (process.env.isDev)
  ? dev
  : {};

const {
  baseUrl = '',
} = config;

const getUrl = (path) => `${baseUrl}${path}`;

const ajax = async(path, params) => {
  const data = await fetch(getUrl(path), params);
  if (data.status >= 300) {
    throw new Error(data.status);
  }
  try {
    const json = await data.json();
    return json;
  } catch (e) {
    return data;
  }
};

const post = (path, payload = {}) => ajax(
  path,
  {
    method: 'POST',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload),
  }
);

const del = (path) => ajax(
  path,
  {
    method: 'DELETE',
    credentials: 'include',
  }
);

const queryParams = (path, params) => {
  if (typeof params !== 'undefined') {
    const urlParams = new URLSearchParams(params);
    return `${path}?${urlParams.toString()}`;
  }
  return path;
};
const get = (path, params) => ajax(
  queryParams(path, params),
  {
    method: 'GET',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
    },
  }
);

// auth
export const signout = () => del('/auth/signout');
export const signin = (payload) => post('/auth/signin', payload);
export const signup = (payload) => post('/auth/signup', payload);

// api
export const listBuckets = () => get('/api/listBuckets');
export const listObjects = (payload) => post('/api/listObjects', payload);
export const getManifest = (params) => get('/api/getManifest', params);
export const setPipeline = (payload) => post('/api/pipeline/set', payload);
export const getPipelines = () => get('/api/pipeline/list');
export const getPipeline = (params) => get('/api/pipeline/get', params);
export const runPipeline = (payload) => post('/api/pipeline/run', payload);
export const getJobs = (params) => get('/api/job/get', params);

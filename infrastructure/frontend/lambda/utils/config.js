const fs = require('fs');
const { resolve } = require('path');
const dotenv = require('dotenv');

const path = resolve(__dirname, '..', '.env');

if (fs.existsSync(path)) {
  dotenv.config({ path });
} else {
  console.error('Couldn\'t find .env file');
}

const {
  AWS_REGION = 'eu-west-1',
  SESSION_TABLE = 'Sessions',
  SESSION_SECRET = 'helloWorld',
  SESSION_NAME = 'sid',
  PORT = 8080,
  NODE_ENV = 'development',
  TABLE_USERS = 'Users',
  TABLE_JOBS = 'Jobs',
} = process.env;

module.exports = {
  aws: {
    region: AWS_REGION,
    dynamoDB: {
      tables: {
        users: TABLE_USERS,
        jobs: TABLE_JOBS,
      },
    },
  },
  session: {
    name: SESSION_NAME,
    store: SESSION_TABLE,
    secret: SESSION_SECRET,
  },
  node: {
    port: PORT,
    env: NODE_ENV,
  },
};

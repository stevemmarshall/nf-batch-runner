const express = require('express');

const router = express.Router();

router.get('/get', require('./get'));
router.get('/list', require('./list'));
router.post('/run', require('./run'));
router.post('/set', require('./set'));

module.exports = router;

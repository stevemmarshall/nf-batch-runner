const express = require('express');

const router = express.Router();

router.get('/get', require('./get'));

module.exports = router;

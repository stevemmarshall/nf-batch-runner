const { clearSession } = require('../../utils');

module.exports = (req, res) => {
  try {
    clearSession(req, res);
    res.sendStatus(200);
  } catch (exception) {
    console.error(exception);
    res.sendStatus(500);
  }
};

# nf-batch-runner

nf-batch-runner is a tool which runs nextflow pipelines using AWS batch.  It is deployed using docker and AWS Cloudformation.

You can [use our cli](https://pypi.org/project/nf-batch-runner) to install everything you need in 15-20 minutes in your own AWS account

![aws architecture diagram](docs/images/architecture.png "How the architecture works")

## Why

You can save time and money by running you analysis in the cloud, but only if you are careful.  AWS Batch is a service which makes 
it easy to turn servers on and off as you need them and Nextflow is able to use it to run your pipelines.

Unfortunatly it isn't simple to setup and Nextflow still needs to run somewhere like your laptop.

nf-batch-runner sets up AWS Batch and some other AWS services so that you can submit you analysis and it will run in the background. 
It comes with an optional frontend which makes it simple for you to submit jobs from your web browser.

You can setup everything in your own AWS account using one command in 15-20 minutes.

## Who uses this

This project is used for the [free pathogen assembly](https://pathogen.watch/upload) service offered by [Pathogenwatch](https://pathogen.watch)

It is also used by our partners in the [Global Health Research Unit](https://ghru.pathogensurveillance.net/)

## Whats next

* Use the CLI [install nf-batch-runner](cli/README.md)
* Install a [new pipeline](docs/new_pipeline.md)
* Use the frontend to [run a pipeline](docs/run_pipeline.md)
* How to [debug errors](docs/debug_pipeline.md)
* Learn more about [how it works](docs/how_it_works.md)
* Read our [internal documentation](docs/internal.md) if you are making or testing changes

## Warnings

* You shouldn't run untrusted pipelines, they get pretty broad access to your AWS account

## TODO

* The tool needs permission to access resources in your account but currently these permissions are broader than you need
* We don't check if two people have requested running the same pipeline with the same inputs
* Change how the ECS agent gets permissions in your account once [this PR is merged](https://github.com/nextflow-io/nextflow/pull/1296)
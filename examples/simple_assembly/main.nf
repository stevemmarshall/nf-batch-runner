#!/usr/bin/env nextflow
// Pipeline version
version = '1.0'
/*

========================================================================================
                          Assembly Pipeline
========================================================================================
 Simple one step assembly
 #### Authors
 Anthony Underwood @bioinformant <au3@sanger.ac.uk>
----------------------------------------------------------------------------------------
*/

def versionMessage(){
  log.info"""
    =========================================
     Assembly Pipeline version ${version}
    =========================================
  """.stripIndent()
}

def helpMessage() {
    log.info"""
    Usage:
    The arguments required for the pipeline are:
    --input_dir      Path to input directory containing the fastq files to be assembled
    --fastq_pattern  The regular expression that will match fastq files e.g '*{R,_}{1,2}*.fastq.gz'
    --output_dir     Path to output directory
    --kmers
   """.stripIndent()
}

//  print help if required
params.help = false
if (params.help){
  versionMessage()
  helpMessage()
  exit 0
}

// Show version number
params.version = false
if (params.version){
  versionMessage()
  exit 0
}

/***************** Setup inputs and channels ************************/
// Defaults for configurable variables
params.input_dir = false
params.output_dir = false
params.fastq_pattern = false
params.kmers = false

if (params.kmers){
  kmers = params.kmers
} else {
  kmers = '21,33,43,53,63,75'
}

//setup output_dir as a variable
output_dir = params.output_dir

// make fastqs channel
input_dir = params.input_dir - ~/\/$/
fastqs = input_dir + '/' + params.fastq_pattern
Channel
    .fromFilePairs( fastqs )
    .ifEmpty { error "Cannot find any reads matching: ${fastqs}" }
    .set { fastqs }



// assemble reads
process spades_assembly {
    publishDir output_dir
    memory '4 GB'

    tag { pair_id }

    input:
    set pair_id, file(reads) from fastqs


    output:
    set pair_id, file("scaffolds.fasta") into scaffolds

    script:
    """
    spades.py --pe1-1 ${reads[0]} --pe1-2 ${reads[1]} --only-assembler  -o . --tmp-dir /tmp/${pair_id}_assembly -k ${kmers} --threads 1 --memory 4

  """
}
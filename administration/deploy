#!/usr/bin/env python3

import json
import json
import json
import os
import re
import shutil
import sys
import tempfile

from contextlib import contextmanager
from subprocess import check_call, check_output, CalledProcessError
from os.path import join

VERSION=sys.argv[1]

@contextmanager
def cleanup():
  temp_dir = tempfile.mkdtemp(prefix=f"nf-batch-runner.{VERSION}.")
  exception = None
  try:
    yield temp_dir
  except e:
    exception = e
  finally:
    shutil.rmtree(temp_dir)
    print("Deleted temp_dir")
    if exception:
      raise exception

def prepare_temp_dir(temp_dir):
  print("Checkout a clean copy of the code and install dependencies", file=sys.stderr)
  git_root = check_output("git rev-parse --show-toplevel".split(" ")).decode('utf8').strip()
  shutil.copytree(join(git_root, ".git"), join(temp_dir, ".git"))
  check_call("git checkout -- .".split(" "), cwd=temp_dir)
  for d in [join(temp_dir, *p) for p in (["infrastructure", "lambdas"], ["infrastructure", "frontend", "lambda"])]:
    check_call("yarn install --production".split(" "), cwd=d)
  check_call("yarn install".split(" "), cwd=join(temp_dir, "infrastructure", "frontend"))

def deploy_infrastructure(temp_dir):
  print("Package and deploy the infrastructure", file=sys.stderr)
  infrastructure_dir = join(temp_dir, "infrastructure")
  check_call(("aws cloudformation package " + \
            "--template-file infrastructure.yml " + \
            "--s3-bucket nf-batch-runner " + \
            "--output-template-file scratch.infrastructure.yml " + \
            f"--s3-prefix {VERSION}/backend").split(" "), cwd=infrastructure_dir)
  check_call(("aws s3 cp " + \
            "--acl public-read " + \
            "--cache-control max-age=0 " + \
            f"scratch.infrastructure.yml s3://nf-batch-runner/{VERSION}/infrastructure.yml").split(" "), cwd=infrastructure_dir)

def deploy_frontend(temp_dir):
  print("Build and deploy the frontend", file=sys.stderr)
  frontend_dir = join(temp_dir, "infrastructure", "frontend")
  check_call("yarn generate".split(" "), cwd=frontend_dir)
  check_call(f"aws s3 sync --cache-control max-age=0 --delete dist s3://nf-batch-runner/{VERSION}/frontend".split(" "), cwd=frontend_dir)

def deploy_cli(temp_dir):
  print("Update the version of the CLI", file=sys.stderr)
  cli_dir = join(temp_dir, "cli")
  with open(join(cli_dir, "setup.py"), "r") as old_f, open(join(cli_dir, "setup.py.new"), "w") as new_f:
    for line in old_f:
      m = re.match('(.+version=").+', line)
      if m:
        prefix = m.groups()[0]
        print(f"{prefix}{VERSION}\",", file=new_f)
      else:
        new_f.write(line)
  os.rename(join(cli_dir, "setup.py.new"), join(cli_dir, "setup.py"))
  check_call("python setup.py sdist bdist_wheel".split(" "), cwd=cli_dir)
  print("Uploading CLI to pypi")
  for i in range(3):
    try:
      check_call("python3 -m twine upload dist/*".split(" "), cwd=cli_dir)
      break
    except CalledProcessError:
      if i >= 2:
        raise
      print("Type uploading to pypi again")

def update_versions(temp_dir):
  print("Update the version in versions.json", file=sys.stderr)
  infrastructure_dir = join(temp_dir, "infrastructure")
  with open(join(infrastructure_dir, "versions.json"), "r") as f:
    versions = json.load(f)
  versions["latest"] = {
    "version": VERSION,
    "frontend": f"s3://nf-batch-runner/{VERSION}/frontend/",
    "backend": f"s3://nf-batch-runner/{VERSION}/infrastructure.yml"
  }
  with open(join(infrastructure_dir, "versions.json"), "w") as f:
    json.dump(versions, f, indent=2)

def update_git(temp_dir):
  print("Make a commit", file=sys.stderr)
  check_call("git commit -a -m".split(" ") + [f"Bump version to {VERSION}"], cwd=temp_dir)

  print("Tag the commit", file=sys.stderr)
  check_call(f"git tag {VERSION}".split(" "), cwd=temp_dir)

  print("Push it to master", file=sys.stderr)
  check_call(f"git push origin".split(" "), cwd=temp_dir)
  check_call(f"git push origin --tags".split(" "), cwd=temp_dir)

def deploy_versions(temp_dir):
  print("Deploy version.json", file=sys.stderr)
  infrastructure_dir = join(temp_dir, "infrastructure")
  versions_file = join(infrastructure_dir, "versions.json")
  check_call(("aws s3 cp " + \
             "--acl public-read " + \
             "--cache-control max-age=0 " + \
             f"{versions_file} s3://nf-batch-runner/versions.json").split(" "), cwd=infrastructure_dir)

def main():
  with cleanup() as temp_dir:
    prepare_temp_dir(temp_dir)
    deploy_infrastructure(temp_dir)
    deploy_frontend(temp_dir)
    
    if VERSION.startswith("test"):
      return
    
    deploy_cli(temp_dir)
    update_versions(temp_dir)
    update_git(temp_dir)
    deploy_versions(temp_dir)

if __name__ == "__main__":
  main()